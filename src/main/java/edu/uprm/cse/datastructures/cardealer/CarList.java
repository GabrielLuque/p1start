package edu.uprm.cse.datastructures.cardealer;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {

	private static CircularSortedDoublyLinkedList<Car> carList=new CircularSortedDoublyLinkedList<>(new CarComparator());
	
	private CarList() {
		CarList.carList = new CircularSortedDoublyLinkedList<>(new CarComparator());
	}
	
	public static CircularSortedDoublyLinkedList<Car> getInstance(){
		return carList;
	}

	
	public static void resetCars() {
		CarList.carList = new CircularSortedDoublyLinkedList<>(new CarComparator());
	}

	
}
