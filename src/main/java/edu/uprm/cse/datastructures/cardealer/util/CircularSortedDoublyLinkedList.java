package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.Car;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {
	private int size = 0;
	private Comparator<E> comparator;
	private Node<E> header;
	Car cars = new Car();
	
	
	private class Node<E>{
		private E data;
		private Node<E> referenceNext;
		private Node<E> referencePrevious;
		
		public Node(E data, Node<E> next, Node<E> previous) {
			this.setData(data);
			this.setReferenceNext(next);
			this.setReferencePrevious(previous);
		}
		
		public Node() {	
		this(null,null,null);		
		
		this.setReferenceNext(this);
		this.setReferencePrevious(this);
		
		}
		
		public Node(E e) {	
			this(e,null,null);		
			
			this.setReferenceNext(this);
			this.setReferencePrevious(this);
			
			}
		
		
		// Getters and Setters

		public E getData() {
			return data;
		}

		public void setData(E data) {
			this.data = data;
		}

		public Node<E> getReferenceNext() {
			return referenceNext;
		}

		public void setReferenceNext(Node<E> referenceNext) {
			this.referenceNext = referenceNext;
		}

		public Node<E> getReferencePrevious() {
			return referencePrevious;
		}

		public void setReferencePrevious(Node<E> referencePrevious) {
			this.referencePrevious = referencePrevious;
		}
	}
	
	
	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		// These names are getting way too long...
		
		private Node<E> nextNode;
		
		public CircularSortedDoublyLinkedListIterator() {
			this.nextNode = (Node<E>) header.getReferenceNext();
		}
		@Override
		public boolean hasNext() {
			return nextNode != header;
		}

		@Override
		public E next() {
			if(this.hasNext()) {
				E result     = this.nextNode.getData();
				this.nextNode= this.nextNode.getReferenceNext();
				
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		
	}
	
	// This constructor doesn't include Comparator maybe delete it?
//	public CircularSortedDoublyLinkedList() {
//		header = new Node<E>();
//		this.comparator = null;
//		this.size = 0;
//	}
	
	
	// Constructor with Comparator
	public CircularSortedDoublyLinkedList(Comparator E) {
		this.header = new Node<E>();
		this.comparator = E;
		this.size = 0;
		
		this.header.setReferenceNext(this.header);
		this.header.setReferencePrevious(this.header);
	}
	
	@Override
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator();
	}

	@Override
	public boolean add(E obj) {
		
		if(obj == null) {
			throw new IllegalArgumentException();
		}
		
		if(this.isEmpty()) {
			Node<E> newNode = new Node<E>(obj,this.getHeader(),this.getHeader());
			
			this.getHeader().setReferenceNext(newNode);
			this.getHeader().setReferencePrevious(newNode);
			
			//System.out.println("PRE-SIZE " + this.size());
			this.size++;
			//System.out.println("POST-SIZE "+this.size());
			return true;
		}
		else {
			Node<E> newNode = new Node<E>(obj,null,null);
			for(Node<E> tempNode = this.getHeader().getReferenceNext();tempNode!= this.getHeader();tempNode =tempNode.getReferenceNext()) {
			
			if(comparator.compare(obj, tempNode.getData()) <= 0)	
			{
				newNode.setReferenceNext(tempNode);
				newNode.setReferencePrevious(tempNode.getReferencePrevious());
				tempNode.getReferencePrevious().setReferenceNext(newNode);

				
				tempNode.setReferencePrevious(newNode);
				
				this.size++;
				return true;
			
			}
			}	
			// System.out.println("=*=*=HEADER REFERENCE PREVIOUS: "+ this.getHeader().getReferencePrevious().getData() +" *=*=*=");
		this.size++;
			
		newNode.setReferenceNext(this.getHeader());
		newNode.setReferencePrevious(this.getHeader().getReferencePrevious());
		this.getHeader().getReferencePrevious().setReferenceNext(newNode);
		this.getHeader().setReferencePrevious(newNode);
		
		
		
		}
		
		
		
		return true;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean remove(E obj) {
		
		if(obj == null) {
			throw new IllegalArgumentException();
		}
		
		if(this.isEmpty()) {
			return false;
		}
		else {
			Node<E> currentNode = this.header.getReferenceNext();
		    for(int i=0;i<this.size();i++) {
		    	 if(currentNode.getData()==obj) {
		 		    currentNode.getReferencePrevious().setReferenceNext(currentNode.getReferenceNext());
		 		    currentNode.getReferenceNext().setReferencePrevious(currentNode.getReferencePrevious());
		 		    
		 		    currentNode.setReferenceNext(null);
		 		    currentNode.setReferencePrevious(null);
		 		    currentNode.setData(null);
		 		    
		 		    currentNode = null;
		 		    this.size--;
		 		    return true;
		 		   }
		    	
		    	
		    	
		    	currentNode = currentNode.getReferenceNext();
		    	
		    }
		    return false;
		  
		}
	}

	@Override
	public boolean remove(int index) {
		
		if(this.isEmpty()) {
			return false;
		}
		
		if(index > this.size() -1) {
			throw new IndexOutOfBoundsException("Bigger than the Linkedlist size."); 
		}
		else if(index < 0) {
			throw new IndexOutOfBoundsException("Index is less than zero. You wanna go reverse in the LinkedList? This ain't Python."); 
		}
	
		else {
			Node<E> currentNode = this.header.getReferenceNext();
		    int i=0;
			while(i!=index) {
				currentNode = currentNode.getReferenceNext();
				i++;
			}
			
			
			
		    currentNode.getReferencePrevious().setReferenceNext(currentNode.getReferenceNext());
		    currentNode.getReferenceNext().setReferencePrevious(currentNode.getReferencePrevious());
		    
		    currentNode.setReferenceNext(null);
 		    currentNode.setReferencePrevious(null);
 		    currentNode.setData(null);
		    
		    currentNode = null;
		    this.size--;
		    return true;
		}
	}

	@Override
	public int removeAll(E obj) {
		if(obj == null) {
			throw new IllegalArgumentException();
		}
		
		int count = 0;
		while(this.remove(obj)) {count++;}
		
		return count;
	}

	@Override
	public E first() {
		return this.getHeader().getReferenceNext().getData();
	}
	
	@Override
	public E last() {
		return this.getHeader().getReferencePrevious().getData();
	}

	@Override
	public E get(int index) {
		if(index > this.size() -1) {
			throw new IndexOutOfBoundsException("Bigger than the Linkedlist size."); 
		}
		else if(index < 0) {
			throw new IndexOutOfBoundsException("Index is less than zero. You wanna go reverse in the LinkedList? This ain't Python."); 
		}
		else {
			Node<E> result = this.getHeader().getReferenceNext();
			
			for(int i=0;i<index;i++) {
			result = result.getReferenceNext();
				
			}	
			
			return result.getData();
		}	
	}

	@Override
	public void clear() {
		while(this.remove(0)) {}
		
		
		
	}

	@Override
	public boolean contains(E e) {
		if(e == null) {
			throw new IllegalArgumentException();
		}
		
		return this.firstIndex(e) > 0;
	}

	@Override
	public boolean isEmpty() {
		return (this.size() == 0);
	}

	@Override
	public int firstIndex(E e) {
		if(e == null) {
			throw new IllegalArgumentException();
		}
		
		if(this.isEmpty()) {
			return -1;
		}
		else {
			Node<E> currentNode = this.header.getReferenceNext();
			
			for(int i = 0; i<this.size();i++) {
				if(currentNode.getData().equals(e)) {
					return i;
				}
				else {
					currentNode = currentNode.getReferenceNext();
				}
			}
		}
		return -1;
	}

	@Override
	public int lastIndex(E e) {
		if(e == null) {
			throw new IllegalArgumentException();
		}
		
//		int indext = 0;
//		Node<E> currentNodeView = this.getHeader().getReferenceNext();
//		System.out.println("======LIST======");
//		while(indext<this.size()) {
//			
//			System.out.println(currentNodeView.getData());
//			currentNodeView = currentNodeView.getReferenceNext();
//			indext++;
//			
//		}
//		System.out.println("======LIST======");
		
		if(this.isEmpty()) {
			return -1;
		}
		else {
			Node<E> currentNode = this.header.getReferencePrevious();
			int index = this.size()-1;
			while(index>0) {
				if(currentNode.getData().equals(e)) {
					return index;
				}
				index--;
				currentNode = currentNode.getReferencePrevious();
			}
		}
		return -1;
	}
	
	
	
	
	
	// Getters and Setters
	
	public Node<E> getHeader() {
		return header;
	}

	public Car getCars() { 
		return cars;
	}

	public void setCars(Car cars) {
		this.cars = cars;
	}
	
	public Comparator<E> getComparator() {
		return comparator;
	}
	
	public void setComparator(Comparator<E> comparator) {
		this.comparator = comparator;
	}

	
	// Helper Method. Never really needed it maybe delete it?
//	
//	private Node<E> getNodeAt(int index){
//		int currIndex = 0;
//		Node<E> temp  = this.getHeader().getReferenceNext();
//		while(index != currIndex) {
//			temp = temp.getReferenceNext();
//			currIndex++;
//		}	
//		return temp;
//	}
	
}
