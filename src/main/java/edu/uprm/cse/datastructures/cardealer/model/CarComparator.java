package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{
	//Implementation of Comparator for Cars
	// Should this be sanitized?
	public int compare(Car firstCar, Car secondCar) {
		
	
	if(firstCar.getCarBrand().compareTo(secondCar.getCarBrand())==0) {
		if(firstCar.getCarModel().compareTo(secondCar.getCarModel())==0) {
			return firstCar.getCarModelOption().compareTo(secondCar.getCarModelOption());
		}
		else {
			return firstCar.getCarModel().compareTo(secondCar.getCarModel());
		}
	}
	
	else {
		return firstCar.getCarBrand().compareTo(secondCar.getCarBrand());
	}
	}	
}
